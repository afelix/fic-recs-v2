# encoding=utf-8
import click

from .application import create_app
from .views import PageTemplate

app = create_app()
with app.app_context():
    import ficrecs.models


@app.cli.command()
def list_routes():
    import urllib.parse
    output = []

    for rule in app.url_map.iter_rules():
        options = {}
        for arg in rule.arguments:
            options[arg] = "[{0}]".format(arg)

        methods = ','.join(rule.methods)

        line = urllib.parse.unquote("{0:40} {1:35} {2}".format(rule.endpoint, methods, str(rule)))
        output.append(line)

    click.echo('Routes: ')
    click.echo('')
    for line in sorted(output):
        click.echo(line)


app.add_url_rule('/template', PageTemplate.endpoint, PageTemplate.as_view(PageTemplate.endpoint))


# DEBUG
@app.route('/test_celery')
def test_celery():
    from .tasks import add
    add.delay(1, 2)
    return 'test'


@app.route('/test_download')
def test_download():
    from celery.app.control import Inspect
    from .application import celery

    i = Inspect(app=celery)
    print(i.registered_tasks())
    print(celery.tasks)

    from .tasks import download_ffn
    download_ffn.delay('5a9a7dd6cd19c77fa9ab2918', ['pdf', 'epub'])
    return 'Downloading...'


@app.route('/test_fics')
def test_fics():
    from .application import mongo
    from gridfs import GridFSBucket

    fs = GridFSBucket(mongo.db)
    output = []

    for grid_data in fs.find():
        output.append(grid_data.filename)
        output.append(str(grid_data.upload_date))
        output.append(grid_data.md5)
        output.append('')

    return '<br />'.join(output)


@app.route('/test_delete_downloads')
def test_delete_downloads():
    from .application import mongo
    from gridfs import GridFSBucket

    fs = GridFSBucket(mongo.db)
    for grid_data in fs.find():
        fs.delete(grid_data._id)

    return 'Deleted all downloads'


@app.route('/test_fic1')
def test_fic1():
    from .application import mongo
    from gridfs import GridFSBucket
    from flask import send_file

    fs = GridFSBucket(mongo.db)

    grid_out = fs.open_download_stream_by_name('asouldreams - Bonding.pdf')

    return send_file(
        grid_out,
        mimetype=grid_out.metadata['content-type'],
        as_attachment=True,
        attachment_filename=grid_out.filename,
        last_modified=grid_out.upload_date
    )


@app.route('/test_fic2')
def test_fic2():
    from .application import mongo
    from gridfs import GridFSBucket
    from flask import send_file

    fs = GridFSBucket(mongo.db)

    grid_out = fs.open_download_stream_by_name('asouldreams - Bonding.epub')

    return send_file(
        grid_out,
        mimetype=grid_out.metadata['content-type'],
        as_attachment=True,
        attachment_filename=grid_out.filename,
        last_modified=grid_out.upload_date
    )
