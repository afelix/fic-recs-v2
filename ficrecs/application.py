# encoding=utf-8
from flask import Flask
from celery import Celery

from flask_pymongo import PyMongo
from flask_cors import CORS

mongo = PyMongo()
celery = None


def create_app():
    app = Flask(__name__)
    app.config.from_object('ficrecs.config_production')

    import os
    app.config['MONGO_URI'] = os.environ.get('MONGO_URI')
    app.config['CELERY_RESULT_BACKEND'] = os.environ.get('MONGO_URI')
    app.config['CELERY_BROKER_URL'] = os.environ.get('MONGO_URI')

    mongo.init_app(app)

    from .bp.api import api_bp
    app.register_blueprint(api_bp, url_prefix='/api')

    from .bp.cdn import cdn_bp
    app.register_blueprint(cdn_bp, url_prefix='/cdn')

    import ficrecs.config_production

    cors = CORS(app, resources={
        '/api/*': {
            # TODO
            "origins": [
                "http://localhost:8080",
                "http://localhost:1880", "http://neela.local:1880",
                ficrecs.config_production.blog
            ],
            "supports_credentials": True
        },
        '/cdn/*': {
            "origins": [
                "http://localhost:8080",
                "http://localhost:1880", "http://neela.local:1880",
                ficrecs.config_production.blog
            ],
        }
    })

    global celery
    celery = make_celery(app)

    return app


def make_celery(app):
    from .utils import OverriddenMongoLoader
    from celery_mongo_broker import Transport

    celery_ = Celery(
        app.import_name,
        backend=app.config['CELERY_RESULT_BACKEND'],
        broker=app.config['CELERY_BROKER_URL'],
        loader=OverriddenMongoLoader,
        config_source={
            'broker_transport': Transport
        }
    )
    celery_.conf.update(app.config)
    TaskBase = celery_.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery_.Task = ContextTask
    return celery_
