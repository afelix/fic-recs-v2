# encoding=utf-8
from flask import Blueprint

BP_NAME = 'api'
api_bp = Blueprint(BP_NAME, __name__)

# Prevent circular imports
from flask_restful import Api
from .resources import (
    OneFandomRes, ListFandomRes,
    OneAuthorRes, ListAuthorRes,
    OneCharacterRes, ListCharacterRes,
    OneShipRes, ListShipRes,
    OneTagRes, ListTagRes,
    OneWorkRes, ListWorkRes,
    Login, SettingsRes
)

api = Api(api_bp)

api.add_resource(Login, '/login')
api.add_resource(SettingsRes, '/settings')
api.add_resource(OneFandomRes, '/fandom/<ObjectId:_id>')
api.add_resource(ListFandomRes, '/fandoms')
api.add_resource(OneAuthorRes, '/author/<ObjectId:_id>')
api.add_resource(ListAuthorRes, '/authors')
api.add_resource(OneCharacterRes, '/character/<ObjectId:_id>')
api.add_resource(ListCharacterRes, '/characters')
api.add_resource(OneShipRes, '/ship/<ObjectId:_id>')
api.add_resource(ListShipRes, '/ships')
api.add_resource(OneTagRes, '/tag/<ObjectId:_id>')
api.add_resource(ListTagRes, '/tags')
api.add_resource(OneWorkRes, '/work/<ObjectId:_id>')
api.add_resource(ListWorkRes, '/works')
