# encoding=utf-8
import os
import base64
import hashlib
import datetime

from flask import jsonify, request, current_app
from flask_restful import abort

from bson.objectid import ObjectId
from marshmallow.exceptions import ValidationError
from pymongo.errors import DuplicateKeyError
import bcrypt

from ...application import mongo
from .schemas import FandomSchema, AuthorSchema, CharacterSchema, ShipSchema, WorkSchema, SettingsSchema, TagSchema
from .utils import bearer_authenticate, Scope, Resource
from . import api_bp


fandom_serialiser = FandomSchema(strict=True)
author_serialiser = AuthorSchema(strict=True)
character_serialiser = CharacterSchema(strict=True)
ship_serialiser = ShipSchema(strict=True)
tag_serialiser = TagSchema(strict=True)
work_serialiser = WorkSchema(strict=True)
settings_serialiser = SettingsSchema(strict=True)


@api_bp.before_app_first_request
def init_serialisers():
    with current_app.app_context():
        settings = mongo.db.settings.find_one({})

    context = {'settings': settings_serialiser.dump(settings).data}

    fandom_serialiser.context = context
    author_serialiser.context = context
    character_serialiser.context = context
    ship_serialiser.context = context
    tag_serialiser.context = context
    work_serialiser.context = context


class OneFandomRes(Resource):
    method_decorators = {
        'put': [bearer_authenticate],
        'delete': [bearer_authenticate]
    }
    scope = {
        'put': Scope.MODIFY,
        'delete': Scope.MODIFY
    }

    def get(self, _id: ObjectId):
        # TODO filtering
        res = mongo.db.fandoms.find_one_or_404({'_id': _id})
        return jsonify(fandom_serialiser.dump(res).data)

    def put(self, _id: ObjectId):
        mongo.db.fandoms.find_one_or_404({'_id': _id})
        json_data = request.get_json()

        try:
            fandom_obj = fandom_serialiser.load(json_data).data
        except ValidationError as err:
            resp = jsonify({
                'success': False,
                'errors': err.messages
            })
            resp.status_code = 400
            return resp

        res1 = mongo.db.fandoms.replace_one({'_id': _id}, fandom_obj)

        # TODO: update fandom name in other collections

        return jsonify({
            'success': True,
            'message': 'Successfully updated fandom {0!r}.'.format(fandom_obj['name'])
        })

    def delete(self, _id: ObjectId):
        # TODO cascade?
        res = mongo.db.fandoms.find_one_and_delete({'_id': _id})
        if not res:
            resp = jsonify({
                'success': False,
                'message': 'Could not find a fandom with id `{0}`'.format(_id)
            })
            resp.status_code = 404
            return resp

        return jsonify({
            'success': True,
            'message': 'Successfully deleted fandom {0!r}.'.format(res['name'])
        })


class ListFandomRes(Resource):
    method_decorators = {
        'post': [bearer_authenticate]
    }
    scope = {
        'post': Scope.MODIFY
    }

    def get(self):
        # TODO filtering
        filter_ = {}
        res = mongo.db.fandoms.find(filter_)
        return jsonify(fandom_serialiser.dump(res, many=True).data)

    def post(self):
        json_data = request.get_json()

        try:
            fandom_obj = fandom_serialiser.load(json_data).data
        except ValidationError as err:
            resp = jsonify({
                'success': False,
                'errors': err.messages
            })
            resp.status_code = 400
            return resp

        try:
            res = mongo.db.fandoms.insert_one(fandom_obj)
            return jsonify({
                'success': True,
                'message': 'Successfully inserted fandom {0!r} as id `{1}`.'.format(fandom_obj['name'],
                                                                                    res.inserted_id),
                'inserted_id': str(res.inserted_id)
            })
        except DuplicateKeyError:
            resp = jsonify({
                'success': False,
                'message': 'There already exists a fandom with name {0!r}'.format(fandom_obj['name'])
            })
            resp.status_code = 400
            return resp


class OneAuthorRes(Resource):
    method_decorators = {
        'put': [bearer_authenticate],
        'delete': [bearer_authenticate]
    }
    scope = {
        'put': Scope.MODIFY,
        'delete': Scope.MODIFY
    }

    def get(self, _id: ObjectId):
        # TODO filtering
        res = mongo.db.authors.find_one_or_404({'_id': _id})
        return jsonify(author_serialiser.dump(res).data)

    def put(self, _id: ObjectId):
        mongo.db.authors.find_one_or_404({'_id': _id})
        json_data = request.get_json()

        try:
            author_obj = author_serialiser.load(json_data).data
        except ValidationError as err:
            resp = jsonify({
                'success': False,
                'errors': err.messages
            })
            resp.status_code = 400
            return resp

        res1 = mongo.db.authors.replace_one({'_id': _id}, author_obj)

        # TODO: check if info in res1 is correct and if it actually replaced things

        # TODO: update author name in other collections (col.update({...}, {...})

        return jsonify({
            'success': True,
            'message': 'Successfully updated author {0!r}.'.format(author_obj['name'])
        })

    def delete(self, _id: ObjectId):
        # TODO cascade?
        res = mongo.db.authors.find_one_and_delete({'_id': _id})

        if not res:
            resp = jsonify({
                'success': False,
                'message': 'Could not find an author with id `{0}`'.format(_id)
            })
            resp.status_code = 404
            return resp

        return jsonify({
            'success': True,
            'message': 'Successfully deleted author {0!r}.'.format(res['name'])
        })


class ListAuthorRes(Resource):
    method_decorators = {
        'post': [bearer_authenticate]
    }
    scope = {
        'post': Scope.MODIFY
    }

    def get(self):
        # TODO filtering
        filter_ = {}
        res = mongo.db.authors.find(filter_)
        return jsonify(author_serialiser.dump(res, many=True).data)

    def post(self):
        json_data = request.get_json()

        try:
            author_obj = author_serialiser.load(json_data).data
        except ValidationError as err:
            resp = jsonify({
                'success': False,
                'errors': err.messages
            })
            resp.status_code = 400
            return resp

        res = mongo.db.authors.insert_one(author_obj)
        return jsonify({
            'success': True,
            'message': 'Successfully inserted author {0!r} as id `{1}`.'.format(author_obj['name'], res.inserted_id),
            'inserted_id': str(res.inserted_id)
        })


class OneCharacterRes(Resource):
    method_decorators = {
        'put': [bearer_authenticate],
        'delete': [bearer_authenticate]
    }
    scope = {
        'put': Scope.MODIFY,
        'delete': Scope.MODIFY
    }

    def get(self, _id: ObjectId):
        # TODO filtering
        res = mongo.db.characters.find_one({'_id': _id})
        return jsonify(character_serialiser.dump(res).data)

    def put(self, _id: ObjectId):
        mongo.db.characters.find_one_or_404({'_id': _id})
        json_data = request.get_json()

        try:
            character_obj = character_serialiser.load(json_data).data
        except ValidationError as err:
            resp = jsonify({
                'success': False,
                'errors': err.messages
            })
            resp.status_code = 400
            return resp

        res1 = mongo.db.characters.replace_one({'_id': _id}, character_obj)

        # TODO: update character name + variant display names in other collections

        return jsonify({
            'success': True,
            'message': 'Successfully updated character {0!r}.'.format(character_obj['name'])
        })

    def delete(self, _id: ObjectId):
        # TODO cascade?
        res = mongo.db.characters.find_one_and_delete({'_id': _id})

        if not res:
            resp = jsonify({
                'success': False,
                'message': 'Could not find a character with id `{0}`'.format(_id)
            })
            resp.status_code = 404
            return resp

        return jsonify({
            'success': True,
            'message': 'Successfully deleted character {0!r}.'.format(res['name'])
        })


class ListCharacterRes(Resource):
    method_decorators = {
        'post': [bearer_authenticate]
    }
    scope = {
        'post': Scope.MODIFY
    }

    def get(self):
        # TODO filtering
        filter_ = {}
        res = mongo.db.characters.find(filter_)
        return jsonify(character_serialiser.dump(res, many=True).data)

    def post(self):
        json_data = request.get_json()

        try:
            character_obj = character_serialiser.load(json_data).data
        except ValidationError as err:
            resp = jsonify({
                'success': False,
                'errors': err.messages
            })
            resp.status_code = 400
            return resp

        res = mongo.db.characters.insert_one(character_obj)
        return jsonify({
            'success': True,
            'message': 'Successfully inserted character {0!r} as id `{1}`.'.format(character_obj['name'],
                                                                                   res.inserted_id),
            'inserted_id': str(res.inserted_id)
        })


class OneShipRes(Resource):
    method_decorators = {
        'put': [bearer_authenticate],
        'delete': [bearer_authenticate]
    }
    scope = {
        'put': Scope.MODIFY,
        'delete': Scope.MODIFY
    }

    def get(self, _id: ObjectId):
        # TODO filtering
        res = mongo.db.ships.find_one({'_id': _id})
        return jsonify(ship_serialiser.dump(res).data)

    def put(self, _id: ObjectId):
        old_data = mongo.db.ships.find_one_or_404({'_id': _id})
        json_data = request.get_json()

        try:
            ship_obj = ship_serialiser.load(json_data).data
        except ValidationError as err:
            resp = jsonify({
                'success': False,
                'errors': err.messages
            })
            resp.status_code = 400
            return resp

        # Small hack to only compute display names in the schema
        display_name = ship_obj['display_name']
        del ship_obj['display_name']

        res1 = mongo.db.ships.replace_one({'_id': _id}, ship_obj)

        # TODO: update display_name in other collections and self

        return jsonify({
            'success': True,
            'message': 'Successfully updated ship {0!r}.'.format(display_name)
        })

    def delete(self, _id: ObjectId):
        # TODO cascade?
        res = mongo.db.ships.find_one_and_delete({'_id': _id})

        if not res:
            resp = jsonify({
                'success': False,
                'message': 'Could not find a ship with id `{0}`.'.format(_id)
            })
            resp.status_code = 404
            return resp

        ship_obj = ship_serialiser.dump(res).data

        return jsonify({
            'success': True,
            'message': 'Successfully deleted ship {0!r}.'.format(ship_obj['display_name'])
        })


class ListShipRes(Resource):
    method_decorators = {
        'post': [bearer_authenticate]
    }
    scope = {
        'post': Scope.MODIFY
    }

    def get(self):
        # TODO filtering
        filter_characters = {}
        filter_exclude = {}

        characters_raw = request.args.getlist('characters[]')

        if characters_raw:
            characters = []
            # prepare ObjectIds...
            for c in characters_raw:
                if '|' in c:
                    # character variant
                    # TODO: Only match specific variant here
                    id_, variant_id = c.split('|')
                    characters.append(ObjectId(id_))
                else:
                    characters.append(ObjectId(c))

            filter_characters = {
                'characters._id': {
                    '$in': characters
                }
            }

        exclude = request.args.get('exclude', '')
        if exclude != '':
            filter_exclude = {
                '_id': {
                    '$ne': ObjectId(exclude)
                }
            }

        if filter_characters != {} and filter_exclude != {}:
            filter_ = {
                '$and': [filter_characters, filter_exclude]
            }
        elif filter_characters != {}:
            filter_ = filter_characters
        elif filter_exclude != {}:
            filter_ = filter_exclude
        else:
            filter_ = {}

        res = mongo.db.ships.find(filter_)
        return jsonify(ship_serialiser.dump(res, many=True).data)

    def post(self):
        json_data = request.get_json()

        try:
            ship_obj = ship_serialiser.load(json_data).data
        except ValidationError as err:
            resp = jsonify({
                'success': False,
                'errors': err.messages
            })
            resp.status_code = 400
            return resp

        # Small hack for computing display_names only in the schema
        display_name = ship_obj['display_name']
        del ship_obj['display_name']

        res = mongo.db.ships.insert_one(ship_obj)
        return jsonify({
            'success': True,
            'message': 'Successfully inserted ship {0!r} as id `{1}`.'.format(display_name, res.inserted_id),
            'inserted_id': str(res.inserted_id)
        })


class OneTagRes(Resource):
    method_decorators = {
        'put': [bearer_authenticate],
        'delete': [bearer_authenticate]
    }
    scope = {
        'put': Scope.MODIFY,
        'delete': Scope.MODIFY
    }

    def get(self, _id: ObjectId):
        # TODO filtering
        res = mongo.db.tags.find_one({'_id': _id})
        return jsonify(tag_serialiser.dump(res).data)

    def put(self, _id: ObjectId):
        old_data = mongo.db.ships.find_one_or_404({'_id': _id})
        json_data = request.get_json()

        try:
            tag_obj = tag_serialiser.load(json_data).data
        except ValidationError as err:
            resp = jsonify({
                'success': False,
                'errors': err.messages
            })
            resp.status_code = 400
            return resp

        res1 = mongo.db.tags.replace_one({'_id': _id}, tag_obj)

        # TODO: update tag in works

        return jsonify({
            'success': True,
            'message': 'Successfully updated tag {0!r}.'.format(tag_obj.tag)
        })

    def delete(self, _id: ObjectId):
        # TODO cascade?
        res = mongo.db.tags.find_one_and_delete({'_id': _id})

        if not res:
            resp = jsonify({
                'success': False,
                'message': 'Could not find a tag with id `{0}`.'.format(_id)
            })
            resp.status_code = 404
            return resp

        tag_obj = tag_serialiser.dump(res).data

        return jsonify({
            'success': True,
            'message': 'Successfully deleted tag {0!r}.'.format(tag_obj['tag'])
        })


class ListTagRes(Resource):
    method_decorators = {
        'post': [bearer_authenticate]
    }
    scope = {
        'post': Scope.MODIFY
    }

    def get(self):
        # TODO filtering
        filter_ = {}
        res = mongo.db.tags.find(filter_)
        return jsonify(tag_serialiser.dump(res, many=True).data)

    def post(self):
        json_data = request.get_json()

        try:
            tag_obj = tag_serialiser.load(json_data).data
        except ValidationError as err:
            resp = jsonify({
                'success': False,
                'errors': err.messages
            })
            resp.status_code = 400
            return resp

        res = mongo.db.tags.insert_one(tag_obj)
        return jsonify({
            'success': True,
            'message': 'Successfully inserted tag {0!r} as id `{1}`.'.format(tag_obj['tag'], res.inserted_id),
            'inserted_id': str(res.inserted_id)
        })


class OneWorkRes(Resource):
    method_decorators = {
        'put': [bearer_authenticate],
        'delete': [bearer_authenticate]
    }
    scope = {
        'put': Scope.MODIFY,
        'delete': Scope.MODIFY
    }

    def get(self, _id: ObjectId):
        # TODO filtering
        res = mongo.db.works.find_one({'_id': _id})
        return jsonify(work_serialiser.dump(res).data)

    def put(self, _id: ObjectId):
        mongo.db.works.find_one_or_404({'_id': _id})
        json_data = request.get_json()

        try:
            work_obj = work_serialiser.load(json_data).data
        except ValidationError as err:
            resp = jsonify({
                'success': False,
                'errors': err.messages
            })
            resp.status_code = 400
            return resp

        # TODO: run bleach over work_obj.summary and work_obj.spoilers

        res1 = mongo.db.works.replace_one({'_id': _id}, work_obj)

        return jsonify({
            'success': True,
            'message': 'Successfully updated work {0!r}.'.format(work_obj['title'])
        })

    def delete(self, _id: ObjectId):
        # TODO cascade?
        res = mongo.db.works.find_one_and_delete({'_id': _id})

        if not res:
            resp = jsonify({
                'success': False,
                'message': 'Could not find a work with id `{0}`.'.format(_id)
            })
            resp.status_code = 404
            return resp

        return jsonify({
            'success': True,
            'message': 'Successfully deleted work {0!r}.'.format(res['title'])
        })


class ListWorkRes(Resource):
    method_decorators = {
        'post': [bearer_authenticate]
    }
    scope = {
        'post': Scope.MODIFY
    }

    def get(self):
        # TODO filtering
        filter_ = {}
        res = mongo.db.works.find(filter_).sort([('sticky', -1), ('pos', 1)])
        return jsonify(work_serialiser.dump(res, many=True).data)

    def post(self):
        json_data = request.get_json()

        try:
            work_obj = work_serialiser.load(json_data).data
        except ValidationError as err:
            resp = jsonify({
                'success': False,
                'errors': err.messages
            })
            resp.status_code = 400
            return resp

        # TODO: run bleach over work_obj.summary and work_obj.spoilers

        for tag in work_obj['tags']:
            if tag['_id'] == '':
                # Create new tag
                try:
                    res = mongo.db.tags.insert_one({
                        'tag': tag['tag']
                    })
                    tag['_id'] = res.inserted_id
                except DuplicateKeyError:
                    # Tag already exists, load ID
                    tag['_id'] = mongo.db.tags.find_one({'tag': tag['tag']})['_id']

        if work_obj['internals']['pos'] == -1:
            work_obj['internals']['pos'] = mongo.db.works.find(
                {
                    'internals.sticky': work_obj['internals']['pos']
                }
            ).count() + 1

        res = mongo.db.works.insert_one(work_obj)
        return jsonify({
            'success': True,
            'message': 'Successfully inserted work {0!r} as id `{1}`.'.format(work_obj['title'], res.inserted_id),
            'inserted_id': str(res.inserted_id)
        })


class SettingsRes(Resource):
    method_decorators = {
        'get': [bearer_authenticate],
        'put': [bearer_authenticate]
    }
    scope = {
        'get': Scope.MODIFY,  # Technically it's not modifying anything, but still require auth
        'put': Scope.MODIFY
    }

    def get(self):
        res = mongo.db.settings.find_one_or_404({})
        return jsonify(settings_serialiser.dump(res).data)

    def put(self):
        current = mongo.db.settings.find_one_or_404({})
        json_data = request.get_json()

        try:
            settings_obj = settings_serialiser.load(json_data).data
        except ValidationError as err:
            resp = jsonify({
                'success': False,
                'errors': err.messages
            })
            resp.status_code = 400
            return resp

        res1 = mongo.db.settings.replace_one({'_id': current['_id']}, settings_obj)

        # Update serialisers with modified settings
        init_serialisers()

        return jsonify({
            'success': True,
            'message': 'Successfully updated settings.'
        })


class Login(Resource):
    def post(self):
        auth = request.authorization
        if not auth:
            abort(401)
        res = mongo.db.users.find_one({'username': auth.username})
        if not res:
            # todo prettify
            abort(401)

        # TODO Lena: Why am I b64 encoding this? That's not in my spec...
        prepared_password = base64.b64encode(hashlib.sha256(auth.password.encode('utf-8')).digest())
        if not bcrypt.checkpw(prepared_password, res['password'].encode('utf-8')):
            # todo prettify
            abort(401)

        # Create bearer token
        token = base64.b64encode(os.urandom(32)).decode('utf-8')
        timestamp = datetime.datetime.utcnow()
        mongo.db.api_tokens.insert_one({
            'user_id': res['_id'],
            'token': token,
            'scope': res['scope'],
            'created_at': timestamp
        })

        return jsonify({
            'success': True,
            'token': token,
            'name': res['username'],
            'scope': res['scope'],
            'expires': int((timestamp + datetime.timedelta(hours=1) - datetime.datetime(1970, 1, 1)).total_seconds()),
            'message': 'Logged in successfully.',
        })
