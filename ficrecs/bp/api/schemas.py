# encoding=utf-8
from marshmallow import Schema, fields, post_dump, post_load, pre_dump, pre_load, validates_schema
from marshmallow.exceptions import ValidationError

from .utils import ObjectIdField, ShipPolyField


class FandomSchema(Schema):
    id = ObjectIdField(dump_only=True, attribute='_id')
    name = fields.String(required=True)
    comments = fields.String()

    @post_dump(pass_many=True)
    def wrap_if_many(self, data, many=False):
        if many:
            return {'fandoms': data, 'count': len(data)}
        return data


class EmbeddedAuthorUrlSchema(Schema):
    primary_url = fields.Url(required=True)
    urls = fields.List(fields.Url(), required=True)


class EmbeddedFandomSchema(Schema):
    id = ObjectIdField(required=True, attribute='_id')
    name = fields.String(required=True)


class AuthorSchema(Schema):
    id = ObjectIdField(dump_only=True, attribute='_id')
    name = fields.String(required=True)
    url = fields.Nested(EmbeddedAuthorUrlSchema, required=True)
    comments = fields.String()
    fandoms = fields.Nested(EmbeddedFandomSchema, many=True, required=True)

    @post_dump(pass_many=True)
    def wrap_if_many(self, data, many=False):
        if many:
            return {'authors': data, 'count': len(data)}
        return data


class EmbeddedCharacterVariantModifiersSchema(Schema):
    gender = fields.String()
    # TODO [DOC]:  Add additional modifiers here that you want to support; format: key = fields.String()


class EmbeddedCharacterVariantSchema(Schema):
    variant_id = fields.Integer(required=True)
    display_name = fields.String(required=True)
    modifiers = fields.Nested(EmbeddedCharacterVariantModifiersSchema)


class CharacterSchema(Schema):
    id = ObjectIdField(dump_only=True, attribute='_id')
    name = fields.String(required=True)
    fandom = fields.Nested(EmbeddedFandomSchema, required=True)
    variants = fields.Nested(EmbeddedCharacterVariantSchema, many=True, required=True)

    @post_dump(pass_many=True)
    def wrap_if_many(self, data, many=False):
        if many:
            return {'characters': data, 'count': len(data)}
        return data


class EmbeddedCharacterSchema(Schema):
    id = ObjectIdField(attribute='_id', required=True)
    variant_id = fields.Integer(required=True, allow_none=True)
    display_name = fields.String(required=True)


class EmbeddedShipSchema(Schema):
    id = ObjectIdField(attribute='_id', required=True)
    display_name = fields.String(required=True)
    display = fields.Boolean(required=True)


class EmbeddedShipPolySchema(Schema):
    sub_ships = fields.Nested(EmbeddedShipSchema, required=True, many=True)
    display = fields.Boolean(required=True)


class ShipSchema(Schema):
    id = ObjectIdField(dump_only=True, attribute='_id')
    characters = fields.Nested(EmbeddedCharacterSchema, required=True, many=True)
    poly = ShipPolyField(EmbeddedShipPolySchema, required=True)
    platonic = fields.Boolean(required=True)
    name = fields.String(required=True)
    display_name = fields.Method(serialize='_display_name', deserialize='_display_name')  # Bit of a hack
    work_display_name = fields.Method(serialize='_work_display_name', dump_only=True)  # Hacky? Definitely

    def _work_display_name(self, obj):
        return ''

    def _display_name(self, obj_or_value):
        return ''

    def _compute_display_name(self, data):
        joiner = ' & ' if data['platonic'] else '/'

        data['display_name'] = joiner.join(c['display_name'] for c in data['characters'])
        if 'settings' in self.context:
            if self.context['settings']['show_ship_name'] and data['name'] != '':
                data['display_name'] += ' ({name})'.format(name=data['name'])
        return data

    def _compute_work_display_name(self, data):
        data['work_display_name'] = {
            'primary_display_name': data['display_name'],
            'other_display_names': [],
        }

        if not data['poly']:
            return data

        ships = []
        for ship in data['poly']['sub_ships']:
            if ship['display']:
                ships.append(ship['display_name'])

        data['work_display_name']['other_display_names'] = ships
        return data

    @post_load
    def compute_display_name_load(self, in_data):
        return self._compute_display_name(in_data)

    @post_dump
    def compute_display_names_dump(self, out_data):
        data = self._compute_display_name(out_data)
        return self._compute_work_display_name(data)

    @post_dump(pass_many=True)
    def wrap_if_many(self, data, many=False):
        if many:
            return {'ships': data, 'count': len(data)}
        return data

    @validates_schema
    def n_characters_check(self, data):
        if len(data['characters']) < 2:
            raise ValidationError('There need to be at least 2 characters specified to form a ship', 'characters')

        if len(data['characters']) > 2 and not data['poly']:
            raise ValidationError('You need to specify a poly relationship', 'poly')


class TagSchema(Schema):
    id = ObjectIdField(attribute='_id', dump_only=True)
    tag = fields.String(required=True)

    @post_dump(pass_many=True)
    def wrap_if_many(self, data, many=False):
        if many:
            return {'tags': data, 'count': len(data)}
        return data


class EmbeddedWorkInternalsSchema(Schema):
    sticky = fields.Boolean(required=True)
    pos = fields.Integer(required=True)


class EmbeddedAuthorSchema(Schema):
    id = ObjectIdField(attribute='_id', required=True)
    name = fields.String(required=True)
    url = fields.Url(required=True, attribute="primary_url")


class EmbeddedTagSchema(Schema):
    id = ObjectIdField(attribute='_id', required=True)
    tag = fields.String(required=True)


class EmbeddedShipWorkDisplayNameSchema(Schema):
    primary_display_name = fields.String(required=True)
    other_display_names = fields.List(fields.String, required=True)


class EmbeddedWorkShipSchema(Schema):
    id = ObjectIdField(attribute='_id', required=True)
    work_display_name = fields.Nested(EmbeddedShipWorkDisplayNameSchema, required=True)


class WorkSchema(Schema):
    id = ObjectIdField(dump_only=True, attribute='_id')
    title = fields.String(required=True)
    url = fields.Url(required=True)
    summary = fields.String(required=True)
    spoilers = fields.String()
    rating = fields.Method(serialize='_serialize_rating', deserialize='_deserialize_rating', required=True)
    complete = fields.Boolean(required=True)
    n_chapters = fields.Integer(required=True)
    n_words = fields.Integer(required=True)
    words_readable = fields.Method(serialize='_words_readable', dump_only=True)
    status = fields.String()
    setting = fields.String()
    tags = fields.Nested(EmbeddedTagSchema, required=True, many=True)
    internals = fields.Nested(EmbeddedWorkInternalsSchema, required=True)

    ships = fields.Nested(EmbeddedWorkShipSchema, required=True, many=True)
    characters = fields.Nested(EmbeddedCharacterSchema, required=True, many=True)
    fandoms = fields.Nested(EmbeddedFandomSchema, required=True, many=True)
    authors = fields.Nested(EmbeddedAuthorSchema, required=True, many=True)

    def _serialize_rating(self, obj):
        return str(obj['rating'])

    def _deserialize_rating(self, value):
        if value not in ['Gen', 'T', 'M', 'PG-13', '15', 'R', 'NC-17', 'Not rated']:
            raise ValidationError('Rating is not a valid rating')
        return value

    def _words_readable(self, obj):
        return '{n_words:,}'.format(n_words=obj['n_words'])

    @post_load
    def n_chapters_check(self, data):
        if data['n_chapters'] < 1:
            raise ValidationError('`n_chapters` needs to be at least 1')

    @post_load
    def uniqueness_constraint(self, data):
        data['tags'] = [dict(s) for s in set(frozenset(d.items()) for d in data['tags'])]

        # data['ships'] = [dict(s) for s in set(frozenset(d.items()) for d in data['ships'])]
        _ships = data['ships']
        data['ships'] = []
        for ship in _ships:
            ship['work_display_name']['other_display_names'] = list(
                set(ship['work_display_name']['other_display_names'])
            )
            if ship not in data['ships']:
                data['ships'].append(ship)

        # TODO [doc] Selfcest? delete the following line
        data['characters'] = [dict(s) for s in set(frozenset(d.items()) for d in data['characters'])]
        data['fandoms'] = [dict(s) for s in set(frozenset(d.items()) for d in data['fandoms'])]
        data['authors'] = [dict(s) for s in set(frozenset(d.items()) for d in data['authors'])]

    @post_dump(pass_many=True)
    def wrap_if_many(self, data, many=False):
        if many:
            return {'works': data, 'count': len(data)}
        return data


class SettingsSchema(Schema):
    show_fandom_character = fields.Boolean(attribute='display_options.character_show_fandom')
    show_ship_name = fields.Boolean(attribute='display_options.ship_show_ship_name')
