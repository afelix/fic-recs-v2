# encoding=utf-8
from functools import wraps
from enum import Flag, auto
import base64
import hashlib

import bcrypt
from flask import request
from flask.views import MethodView

from marshmallow import fields
from marshmallow.exceptions import ValidationError
from bson.objectid import ObjectId
from bson.errors import InvalidId
from flask_restful import abort, Mapping, ResponseBase, OrderedDict, unpack

from ...application import mongo


# Small modification to flask_restful base class
class Resource(MethodView):
    """
    Represents an abstract RESTful resource. Concrete resources should
    extend from this class and expose methods for each supported HTTP
    method. If a resource is invoked with an unsupported HTTP method,
    the API will return a response with status 405 Method Not Allowed.
    Otherwise the appropriate method is called and passed all arguments
    from the url rule used when adding the resource to an Api instance. See
    :meth:`~flask_restful.Api.add_resource` for details.
    """
    representations = None
    method_decorators = []

    def dispatch_request(self, *args, **kwargs):

        # Taken from flask
        meth = getattr(self, request.method.lower(), None)
        if meth is None and request.method == 'HEAD':
            meth = getattr(self, 'get', None)
        assert meth is not None, 'Unimplemented method %r' % request.method

        if isinstance(self.method_decorators, Mapping):
            decorators = self.method_decorators.get(request.method.lower(), [])
        else:
            decorators = self.method_decorators

        for decorator in decorators:
            meth = decorator(meth)

        # [Lena: only thing changed to support scope in authentication]
        if len(decorators) > 0:
            resp = meth(self, *args, **kwargs)
        else:
            resp = meth(*args, **kwargs)

        if isinstance(resp, ResponseBase):  # There may be a better way to test
            return resp

        representations = self.representations or OrderedDict()

        mediatype = request.accept_mimetypes.best_match(representations, default=None)
        if mediatype in representations:
            data, code, headers = unpack(resp)
            resp = representations[mediatype](data, code, headers)
            resp.headers['Content-Type'] = mediatype
            return resp

        return resp


class Scope(Flag):
    READ = auto()
    MODIFY = auto()
    DEBUG = auto()


class ObjectIdField(fields.Field):
    def _serialize(self, value, attr, obj):
        return str(value)

    def _deserialize(self, value, attr, data):
        if value == '':
            # New object, pass silently so it can be handled in the backend
            return value

        try:
            return ObjectId(value)
        except (InvalidId, TypeError):
            raise ValidationError('Invalid ObjectId')


class ShipPolyField(fields.Field):
    #: Values that will (de)serialize to `True`. If an empty set, any non-falsy
    #  value will deserialize to `True`.
    truthy = {'t', 'T', 'true', 'True', 'TRUE', '1', 1, True}
    #: Values that will (de)serialize to `False`.
    falsy = {'f', 'F', 'false', 'False', 'FALSE', '0', 0, 0.0, False}

    default_error_messages = {
        'invalid_bool': 'This field only accepts false or a valid poly schema',
        'invalid_schema': 'Errors found in the {schema!r} schema: {errors}'
    }

    def __init__(self, poly_schema, **metadata):
        self.poly_schema = poly_schema
        super().__init__(**metadata)

    def _serialize(self, value, attr, obj):
        try:
            if value in self.falsy:
                return False
            if value in self.truthy:
                self.fail('invalid_bool')
        except TypeError:
            pass

        try:
            schema = self.poly_schema(strict=True)
            return schema.dump(value).data
        except ValidationError as err:
            self.fail('invalid_schema', schema=self.poly_schema.__class__.__name__, errors=err)

    def _deserialize(self, value, attr, data):
        try:
            if value in self.falsy:
                return False
            if value in self.truthy:
                self.fail('invalid_bool')
        except TypeError:
            pass

        try:
            schema = self.poly_schema(strict=True)
            return schema.load(value).data
        except ValidationError as err:
            self.fail('invalid_schema', schema=self.poly_schema.__class__.__name__, errors=err)


# Auth utils
def bearer_authenticate(func):
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        if 'Authorization' not in request.headers:
            abort(401)
        if request.headers['Authorization'][:7] != 'Bearer ':
            abort(401)

        token = request.headers['Authorization'][7:]
        scope = Scope(self.scope[request.method.lower()])
        res = mongo.db.api_tokens.find_one({'token': token})
        if not res:
            # invalid token
            abort(401)

        if scope in Scope(res['scope']):
            # auth right and enough permissions
            return func(*args, **kwargs)

    return wrapper


def create_user(username, password, scope):
    # TODO Lena: Why am I b64 encoding this? That's not in my spec...
    password = base64.b64encode(hashlib.sha256(password.encode('utf-8')).digest())
    mongo.db.users.insert_one({
        'username': username,
        'password': bcrypt.hashpw(password, bcrypt.gensalt()).decode('utf-8'),
        'scope': scope.value
    })

