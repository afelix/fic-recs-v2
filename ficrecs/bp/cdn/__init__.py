# encoding=utf-8
from flask import Blueprint
from .views import JSApp, JSManifest, JSVendor, CSSApp, JSMapApp, JSMapManifest, JSMapVendor, CSSMapApp, TinyMCE

BP_NAME = 'cdn'

cdn_bp = Blueprint(BP_NAME, __name__)

cdn_bp.add_url_rule('/css/app.css', CSSApp.endpoint, view_func=CSSApp.as_view(CSSApp.endpoint))
cdn_bp.add_url_rule('/css/app.<string:hash>.css.map', CSSMapApp.endpoint,
                    view_func=CSSMapApp.as_view(CSSMapApp.endpoint))
cdn_bp.add_url_rule('/js/app.js', JSApp.endpoint, view_func=JSApp.as_view(JSApp.endpoint))
cdn_bp.add_url_rule('/js/app.<string:hash>.js.map', JSMapApp.endpoint,
                    view_func=JSMapApp.as_view(JSMapApp.endpoint))
cdn_bp.add_url_rule('/js/manifest.js', JSManifest.endpoint, view_func=JSManifest.as_view(JSManifest.endpoint))
cdn_bp.add_url_rule('/js/manifest.<string:hash>.js.map', JSMapManifest.endpoint,
                    view_func=JSMapManifest.as_view(JSMapManifest.endpoint))
cdn_bp.add_url_rule('/js/vendor.js', JSVendor.endpoint, view_func=JSVendor.as_view(JSVendor.endpoint))
cdn_bp.add_url_rule('/js/vendor.<string:hash>.js.map', JSMapVendor.endpoint,
                    view_func=JSMapVendor.as_view(JSMapVendor.endpoint))
cdn_bp.add_url_rule('/tinymce/<path:filename>', TinyMCE.endpoint, view_func=TinyMCE.as_view(TinyMCE.endpoint))
