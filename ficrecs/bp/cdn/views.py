# encoding=utf-8
from flask import abort, send_file, send_from_directory, safe_join
from flask.views import View

from ...config_production import cdn, cdn_directory


class CSSApp(View):
    endpoint = 'css_app'

    def dispatch_request(self):
        return send_file(
            safe_join(
                cdn_directory, 'css',
                'app.{hash}.css'.format(hash=cdn['hashes_latest']['css']['app'])
            )
        )


class CSSMapApp(View):
    endpoint = 'css_map_app'

    def dispatch_request(self, hash):
        return send_from_directory(safe_join(cdn_directory, 'css'), 'app.{hash}.css.map'.format(hash=hash))


class JSApp(View):
    endpoint = 'js_app'

    def dispatch_request(self):
        return send_file(
            safe_join(
                cdn_directory, 'js',
                'app.{hash}.js'.format(hash=cdn['hashes_latest']['js']['app'])
            )
        )


class JSMapApp(View):
    endpoint = 'js_map_app'

    def dispatch_request(self, hash):
        return send_from_directory(safe_join(cdn_directory, 'js'), 'app.{hash}.js.map'.format(hash=hash))


class JSManifest(View):
    endpoint = 'js_manifest'

    def dispatch_request(self):
        return send_file(
            safe_join(
                cdn_directory, 'js',
                'manifest.{hash}.js'.format(hash=cdn['hashes_latest']['js']['manifest'])
            )
        )


class JSMapManifest(View):
    endpoint = 'js_map_manifest'

    def dispatch_request(self, hash):
        return send_from_directory(safe_join(cdn_directory, 'js'), 'manifest.{hash}.js.map'.format(hash=hash))


class JSVendor(View):
    endpoint = 'js_vendor'

    def dispatch_request(self):
        return send_file(
            safe_join(
                cdn_directory, 'js',
                'vendor.{hash}.js'.format(hash=cdn['hashes_latest']['js']['vendor'])
            )
        )


class JSMapVendor(View):
    endpoint = 'js_map_vendor'

    def dispatch_request(self, hash):
        return send_from_directory(safe_join(cdn_directory, 'js'), 'vendor.{hash}.js.map'.format(hash=hash))


class TinyMCE(View):
    endpoint = 'tinymce'

    def dispatch_request(self, filename):
        mimetype = None
        if filename[-8:] == '.min.css':
            mimetype = 'text/css'
        return send_from_directory(safe_join(cdn_directory, 'tinymce'), filename, mimetype=mimetype)
