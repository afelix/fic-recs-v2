# encoding=utf-8
from .application import create_app, celery

app = create_app()
