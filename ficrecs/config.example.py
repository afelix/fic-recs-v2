# encoding=utf-8
mongo_details =	{
    'host': '127.0.0.1',
    'user': '',
    'password':	'',
    'auth_database': '',
    'auth_mechanism': 'SCRAM-SHA-1'
}

mongo_version = '3.6'

MONGO_URI = 'mongodb://{user}:{password}@{host}/{auth_database}?authMechanism={auth_mechanism}'.format(**mongo_details)