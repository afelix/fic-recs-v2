# encoding=utf-8
mongo_version = '3.4'
blog_short = 'flight-of-the-felix'
blog = 'https://{blog_short}.tumblr.com'.format(blog_short=blog_short)
cdn_directory = 'CDN'
cdn = {
    'hashes_latest': {
        'css': {
            'app': 'e25abadf06b30e658e79adcfd4cc6031'
        },
        'js': {
            'app': '30661b75c6bd4718b22e',
            'manifest': '7175246ef0cecbc43ab0',
            'vendor': '80ae33ba93b3d487e437',
        }
    },
    'tinymce': {
        'skins': {
            'lightgray': {
                'fonts': [
                    'tinymce.eot', 'tinymce.svg', 'tinymce.ttf', 'tinymce.woff', 'tinymce-mobile.woff',
                    'tinymce-small.eot', 'tinymce-small.svg', 'tinymce-small-ttf', 'tinymce-small.woff'
                ],
                'img': [
                    'anchor.gif', 'loader.gif', 'object.gif', 'trans.gif'
                ],
                '_': [
                    'content.inline.min.css', 'content.min.css', 'content.mobile.min.css', 'skin.min.css',
                    'skin.mobile.min.css'
                ]
            }
        },
        'themes': {
            'inlite': ['index.js', 'theme.js', 'theme.min.js'],
            'mobile': ['index.js', 'theme.js', 'theme.min.js'],
            'modern': ['index.js', 'theme.js', 'theme.min.js']
        }
    }
}

CELERY_IMPORTS = ['ficrecs.tasks']
CELERY_TASK_SERIALIZER = 'json'
