# encoding=utf-8
from .application import mongo

validator_fandom = {
    '$jsonSchema': {
        'bsonType': 'object',
        'required': ['_id', 'name'],
        'additionalProperties': False,
        'properties': {
            '_id': {
                'bsonType': 'objectId'
            },
            'name': {
                'bsonType': 'string',
                'description': 'must be a string and is required'
            },
            'comments': {
                'bsonType': 'string',
                'description': 'must be a string and is not required'
            }
        }
    }
}

validator_author = {
    '$jsonSchema': {
        'bsonType': 'object',
        'required': ['_id', 'name', 'fandoms', 'url'],
        'additionalProperties': False,
        'properties': {
            '_id': {
                'bsonType': 'objectId'
            },
            'name': {
                'bsonType': 'string',
                'description': 'must be a string and is required'
            },
            'url': {
                'bsonType': 'object',
                'description': 'must be an object and is required',
                'additionalProperties': False,
                'required': ['primary_url', 'urls'],
                'properties': {
                    'primary_url': {
                        'bsonType': 'string',
                        'description': 'must be a string and is required, but can be empty'
                    },
                    'urls': {
                        'bsonType': 'array',
                        'description': 'must be an array of unique strings and is required, but can be empty',
                        'additionalItems': False,
                        'items': {
                            'bsonType': 'string',
                            'description': 'must be a string and is not required'
                        }
                    }
                }
            },
            'comments': {
                'bsonType': 'string',
                'description': 'must be a string and is not required'
            },
            'fandoms': {
                'bsonType': 'array',
                'description': 'must be an array of fandom references',
                'additionalItems': False,
                'items': {
                    'bsonType': 'object',
                    'required': ['_id', 'name'],
                    'additionalProperties': False,
                    'properties': {
                        '_id': {
                            'bsonType': 'objectId',
                            'description': 'must be an objectid and be a reference to the `db.fandoms` and is required'
                        },
                        'name': {
                            'bsonType': 'string',
                            'description': 'must be a string and is required'
                        }
                    }
                }
            }
        }
    }
}

validator_character = {
    '$jsonSchema': {
        'bsonType': 'object',
        'required': ['_id', 'name', 'fandom'],
        'additionalProperties': False,
        'properties': {
            '_id': {
                'bsonType': 'objectId'
            },
            'name': {
                'bsonType': 'string',
                'description': 'must be a string and is required'
            },
            'fandom': {
                'bsonType': 'object',
                'description': 'must be a fandom reference',
                'required': ['_id', 'name'],
                'properties': {
                    '_id': {
                        'bsonType': 'objectId',
                        'description': 'must be an objectId pointing to `db.fandoms` and is required'
                    },
                    'name': {
                        'bsonType': 'string',
                        'description': 'must be a string and is required'
                    }
                }
            },
            'variants': {
                'bsonType': 'array',
                'description': 'must be an array of unique variants',
                'additionalItems': False,
                'uniqueItems': True,
                'items': {
                    'bsonType': 'object',
                    'required': ['variant_id', 'modifiers', 'display_name'],
                    'additionalProperties': False,
                    'properties': {
                        'variant_id': {
                            'bsonType': 'int',
                            'description': 'must be an integer and is required'
                        },
                        'display_name': {
                            'bsonType': 'string',
                            'description': 'must be a string and is required'
                        },
                        'modifiers': {
                            'bsonType': 'object',
                            'additionalProperties': {
                                'bsonType': 'string',
                                'description': 'additional modifiers, optional'
                            },
                            'properties': {
                                'gender': {
                                    'bsonType': 'string',
                                    'description': 'must be a string and is not required'
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

validator_ship = {
    '$jsonSchema': {
        'bsonType': 'object',
        'required': ['_id', 'characters', 'platonic', 'poly', 'name'],
        'additionalProperties': False,
        'properties': {
            '_id': {
                'bsonType': 'objectId'
            },
            'characters': {
                'bsonType': 'array',
                'additionalItems': False,
                'uniqueItems': True,  # TODO [doc] Selfcest? Set to false
                'minItems': 2,
                'description': 'set of characters to include in this ship',
                'items': {
                    'bsonType': 'object',
                    'required': ['_id', 'variant_id', 'display_name'],
                    'additionalProperties': False,
                    'properties': {
                        '_id': {
                            'bsonType': 'objectId',
                            'description': 'must be an objectId pointing to `db.characters` and is required'
                        },
                        'variant_id': {
                            'bsonType': ['int', 'null'],
                            'description': 'must be an integer or null and is required'
                        },
                        'display_name': {
                            'bsonType': 'string',
                            'description': 'must be a string and is required'
                        }
                    }
                }
            },
            'platonic': {
                'bsonType': 'bool',
                'description': 'must be a boolean and is required'
            },
            'name': {
                'bsonType': 'string',
                'description': 'must be a string and is required, but can be an empty string'
            },
            'poly': {
                'bsonType': ['bool', 'object'],
                'description': 'must be a boolean (false) or an object and is required',
                'additionalProperties': False,
                'required': ['sub_ships', 'display'],
                'properties': {
                    'display': {
                        'bsonType': 'bool',
                        'description': 'must be a boolean and is required'
                    },
                    'sub_ships': {
                        'bsonType': 'array',
                        'description': 'must be an array of unique ships',
                        'additionalItems': False,
                        'uniqueItems': True,
                        'items': {
                            'bsonType': 'object',
                            'additionalProperties': False,
                            'description': 'must be an object and is required',
                            'required': ['_id', 'display_name', 'display'],
                            'properties': {
                                '_id': {
                                    'bsonType': 'objectId',
                                    'description': 'must be an objectId pointing to `db.ships` and is required'
                                },
                                'display': {
                                    'bsonType': 'bool',
                                    'description': 'must be a boolean and is required'
                                },
                                'display_name': {
                                    'bsonType': 'string',
                                    'description': 'must be a string and is required'
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

validator_tag = {
    '$jsonSchema': {
        'bsonType': 'object',
        'required': ['_id', 'tag'],
        'additionalProperties': False,
        'properties': {
            '_id': {
                'bsonType': 'objectId'
            },
            'tag': {
                'bsonType': 'string',
                'description': 'must be a string and is required'
            }
        }
    }
}

validator_work = {
    '$jsonSchema': {
        'bsonType': 'object',
        'required': ['_id', 'title', 'url', 'summary', 'rating', 'complete', 'n_chapters', 'n_words', 'internals',
                     'fandoms', 'authors'],
        'additionalProperties': False,
        'properties': {
            '_id': {
                'bsonType': 'objectId'
            },
            'title': {
                'bsonType': 'string',
                'description': 'must be a string and is required'
            },
            'url': {
                'bsonType': 'string',
                'description': 'must be a string and is required'
            },
            'summary': {
                'bsonType': 'string',
                'description': 'must be a string and is required'
            },
            'spoilers': {
                'bsonType': 'string',
                'description': 'must be a string and is not required'
            },
            'rating': {
                'enum': ['Gen', 'T', 'M', 'PG-13', '15', 'R', 'NC-17', 'Not rated'],
                'description': 'must be one of the enum values and is required'
            },
            'complete': {
                'bsonType': 'bool',
                'description': 'must be a boolean and is required'
            },
            'n_chapters': {
                'bsonType': 'int',
                'description': 'must be an integer and is required',
                'minimum': 1
            },
            'n_words': {
                'bsonType': 'int',
                'description': 'must be an integer and is required',
                'minimum': 1
            },
            'status': {
                'bsonType': 'string',
                'description': 'must be a string and is not required'
            },
            'setting': {
                'bsonType': 'string',
                'description': 'must be a string and is not required'
            },
            'tags': {
                'bsonType': 'array',
                'description': 'must be an array of unique strings and is required, but can be empty',
                'additionalItems': False,
                'uniqueItems': True,
                'items': {
                    'bsonType': 'object',
                    'description': 'set of tags in this work',
                    'additionalProperties': False,
                    'required': ['_id', 'tag'],
                    'properties': {
                        '_id': {
                            'bsonType': 'objectId',
                            'description': 'must be an objectId and is required'
                        },
                        'tag': {
                            'bsonType': 'string',
                            'description': 'must be a string and is not required'
                        }
                    }
                }
            },
            'internals': {
                'bsonType': 'object',
                'additionalProperties': False,
                'required': ['sticky', 'pos'],
                'properties': {
                    'sticky': {
                        'bsonType': 'bool',
                        'description': 'must be a boolean and is required'
                    },
                    'pos': {
                        'bsonType': 'int',
                        'description': 'must be an integer and is required'
                    }
                }
            },
            'ships': {
                'bsonType': 'array',
                'additionalItems': False,
                'uniqueItems': True,
                'description': 'must be an array of unique ships',
                'items': {
                    'bsonType': 'object',
                    'description': 'set of ships in this work',
                    'additionalProperties': False,
                    'required': ['_id', 'work_display_name'],
                    'properties': {
                        '_id': {
                            'bsonType': 'objectId',
                            'description': 'must be an objectId pointing to `db.ships` and is required'
                        },
                        'work_display_name': {
                            'bsonType': 'object',
                            'description': 'must be an object and is required',
                            'required': ['primary_display_name', 'other_display_names'],
                            'additionalProperties': False,
                            'properties': {
                                'primary_display_name': {
                                    'bsonType': 'string',
                                    'description': 'must be a string and is required'
                                },
                                'other_display_names': {
                                    'bsonType': 'array',
                                    'description': 'set of other display names of unique sub ships',
                                    'additionalItems': False,
                                    'uniqueItems': True,
                                    'items': {
                                        'bsonType': 'string',
                                        'description': 'must be a string and is not required'
                                    }
                                }
                            }
                        }
                    }
                }
            },
            'characters': {
                'bsonType': 'array',
                'additionalItems': False,
                'uniqueItems': True,
                'description': 'must be an array of unique additional characters',
                'items': {
                    'bsonType': 'object',
                    'required': ['_id', 'variant_id', 'display_name'],
                    'additionalProperties': False,
                    'properties': {
                        '_id': {
                            'bsonType': 'objectId',
                            'description': 'must be an objectId pointing to `db.characters` and is required'
                        },
                        'variant_id': {
                            'bsonType': ['int', 'null'],
                            'description': 'must be an integer or null and is required'
                        },
                        'display_name': {
                            'bsonType': 'string',
                            'description': 'must be a string and is required'
                        }
                    }
                }
            },
            'fandoms': {
                'bsonType': 'array',
                'description': 'must be an array of unique fandoms and is required',
                'additionalItems': False,
                'uniqueItems': True,
                'minItems': 1,
                'items': {
                    'bsonType': 'object',
                    'required': ['_id', 'name'],
                    'additionalProperties': False,
                    'properties': {
                        '_id': {
                            'bsonType': 'objectId',
                            'description': 'must be an objectid and be a reference to the `db.fandoms` and is required'
                        },
                        'name': {
                            'bsonType': 'string',
                            'description': 'must be a string and is required'
                        }
                    }
                }
            },
            'authors': {
                'bsonType': 'array',
                'description': 'must be an array of unique authors and is required',
                'additionalItems': False,
                'uniqueItems': True,
                'minItems': 1,
                'items': {
                    'bsonType': 'object',
                    'required': ['_id', 'name', 'primary_url'],
                    'additionalProperties': False,
                    'properties': {
                        '_id': {
                            'bsonType': 'objectId',
                            'description': 'must be an objectid and be a reference to the `db.authors` and is required'
                        },
                        'name': {
                            'bsonType': 'string',
                            'description': 'must be a string and is required'
                        },
                        'primary_url': {
                            'bsonType': 'string',
                            'description': 'must be a string and is required'
                        }
                    }
                }
            }
        }
    }
}

validator_user = {
    '$jsonSchema': {
        'bsonType': 'object',
        'required': ['_id', 'username', 'password', 'scope'],
        'additionalProperties': False,
        'properties': {
            '_id': {
                'bsonType': 'objectId',
            },
            'username': {
                'bsonType': 'string',
                'description': 'must be a unique string and is required'
            },
            'password': {
                'bsonType': 'string',
                'description': 'must be a bcrypt hashed password string and is required'
            },
            'scope': {
                'bsonType': 'int',
                'description': 'must be an integer flag and is required'
            }
        }
    }
}

validator_api_token = {
    '$jsonSchema': {
        'bsonType': 'object',
        'required': ['_id', 'user_id', 'token', 'scope', 'created_at'],
        'additionalProperties': False,
        'properties': {
            '_id': {
                'bsonType': 'objectId'
            },
            'user_id': {
                'bsonType': 'objectId',
                'description': 'must be an objectid pointing to `db.users` and is required'
            },
            'token': {
                'bsonType': 'string',
                'description': 'must be a bearer token as string and is required'
            },
            'scope': {
                'bsonType': 'int',
                'description': 'must be an integer flag and is required'
            },
            'created_at': {
                'bsonType': 'date',
                'description': 'must be a date and is required'
            }
        }
    }
}

validator_settings = {
    '$jsonSchema': {
        'bsonType': 'object',
        'required': ['_id', 'display_options'],
        'additionalProperties': False,
        'properties': {
            '_id': {
                'bsonType': 'objectId'
            },
            'display_options': {
                'bsonType': 'object',
                'additionalProperties': False,
                'properties': {
                    'character_show_fandom': {
                        'bsonType': 'bool',
                        'description': 'must be a boolean and is false by default'
                    },
                    'ship_show_ship_name': {
                        'bsonType': 'bool',
                        'description': 'must be a boolean and is false by default'
                    }
                }
            }
        }
    }
}

if 'fandoms' not in mongo.db.collection_names():
    # Fresh install
    from .config import mongo_version

    if mongo_version == '3.6':
        # Create collections with validation
        mongo.db.create_collection('fandoms', validator=validator_fandom)
        mongo.db.create_collection('authors', validator=validator_author)
        mongo.db.create_collection('characters', validator=validator_character)
        mongo.db.create_collection('ships', validator=validator_ship)
        mongo.db.create_collection('tags', validator=validator_tag)
        mongo.db.create_collection('works', validator=validator_work)
        mongo.db.create_collection('users', validator=validator_user)
        mongo.db.create_collection('api_tokens', validator=validator_api_token)
        mongo.db.create_collection('settings', validator=validator_settings)
    else:
        mongo.db.create_collection('fandoms')
        mongo.db.create_collection('authors')
        mongo.db.create_collection('characters')
        mongo.db.create_collection('ships')
        mongo.db.create_collection('tags')
        mongo.db.create_collection('works')
        mongo.db.create_collection('users')
        mongo.db.create_collection('api_tokens')
        mongo.db.create_collection('settings')

    # Create indices:
    # TODO
    import pymongo
    mongo.db.fandoms.create_index([('name', pymongo.ASCENDING)], unique=True)
    mongo.db.authors.create_index([('name', pymongo.ASCENDING), ('url.primary_url', pymongo.ASCENDING)], unique=True)
    mongo.db.characters.create_index([('name', pymongo.ASCENDING), ('fandom._id', pymongo.ASCENDING)], unique=True)
    mongo.db.ships.create_index([('characters.display_name', pymongo.ASCENDING)])
    mongo.db.tags.create_index([('tag', pymongo.ASCENDING)], unique=True)

    mongo.db.api_tokens.create_index([('created_at', pymongo.ASCENDING)], expireAfterSeconds=3600)

    # Intialise settings
    mongo.db.settings.insert_one({
        'display_options': {
            'character_show_fandom': False,
            'ship_show_ship_name': False
        }
    })
