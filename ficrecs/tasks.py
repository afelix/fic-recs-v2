# encoding=utf-8
import re
from io import BytesIO
import os

import pymongo
from gridfs import GridFSBucket
from bson.objectid import ObjectId

from ffnparser import Parser, Handler, HandlerEpub, HandlerPDF

from .application import celery
from .bp.api.schemas import WorkSchema


_work_serializer = WorkSchema(strict=True)
_pattern = r'https:\/\/www\.fanfiction\.net\/s\/(?P<fic_id>[0-9]+)\/(?P<n_chapter>[0-9]+)\/?(?P<fic_title>[A-Za-z\-]*)'


@celery.task
def add(x, y):
    return x + y


@celery.task
def download_ffn(work_id, handlers):
    if not isinstance(work_id, ObjectId):
        work_id = ObjectId(work_id)

    db = pymongo.MongoClient(os.environ['MONGO_URI']).ficrecs

    # Get information on work first...
    res = db.works.find_one({'_id': work_id})
    if not res:
        raise ValueError('Fic not found')

    work = _work_serializer.dump(res).data
    author = ', '.join([a['name'] for a in work['authors']])
    title = work['title']
    match = re.match(_pattern, work['url'])

    if not match:
        # Not a FFN url
        raise ValueError('Not a fanfiction.net fic')

    if not isinstance(handlers, list):
        # TODO
        pass

    fs = GridFSBucket(db)

    handlers_ = []

    for handler in handlers:
        if handler == 'pdf':
            handlers_.append(Handler(handler=HandlerPDF(), fp=BytesIO()))
        elif handler == 'epub':
            handlers_.append(Handler(handler=HandlerEpub(), fp=BytesIO()))

    parser = Parser(match.group('fic_id'), handlers=handlers_)
    parser.parse()

    for handler in handlers_:
        if isinstance(handler.handler, HandlerEpub):
            epub_in = fs.open_upload_stream('{author} - {title}.epub'.format(author=author, title=title), metadata={
                'content-type': 'application/epub+zip',
                'work_id': work_id,
            })
            epub_in.write(handler.fp.getvalue())
            epub_in.close()
        elif isinstance(handler.handler, HandlerPDF):
            pdf_in = fs.open_upload_stream('{author} - {title}.pdf'.format(author=author, title=title), metadata={
                'content-type': 'application/pdf',
                'work_id': work_id,
            })
            pdf_in.write(handler.fp.getvalue())
            pdf_in.close()

        handler.fp.close()
