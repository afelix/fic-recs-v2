# -*- coding: utf-8 -*-
from celery.loaders.app import AppLoader

__all__ = ('AppLoader',)


class OverriddenMongoLoader(AppLoader):
    override_backends = {
        'mongodb': 'celery_mongo_backend:MongoBackend'
    }
