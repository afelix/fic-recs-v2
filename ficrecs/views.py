# encoding=utf-8
from flask import render_template
from flask.views import View

from .config_production import blog_short


class PageTemplate(View):
    endpoint = 'page_template'

    def dispatch_request(self):
        return render_template('page_template.html', blog=blog_short)
