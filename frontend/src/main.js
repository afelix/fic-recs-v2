import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

import fontawesome from '@fortawesome/fontawesome';
import { faEdit, faTrashAlt } from '@fortawesome/fontawesome-free-regular';

import TinyMCE from 'tinymce';

import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import App from './App';
import router from './router';
import store from './store';

fontawesome.library.add([faEdit, faTrashAlt]);

Vue.use(BootstrapVue);
Vue.use(TinyMCE);
Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
});
