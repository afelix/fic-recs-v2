import Vue from 'vue';
import Router from 'vue-router';
import Menu from '@/components/FRMenu';
import Test from '@/components/Test';
import Index from '@/components/Index';
import ListFandoms from '@/components/ListFandoms';
import ListAuthors from '@/components/ListAuthors';
import ListCharacters from '@/components/ListCharacters';
import ListShips from '@/components/ListShips';
import ListWorks from '@/components/ListWorks';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      components: {
        default: Index,
        menu: Menu,
      },
    },
    {
      path: '/test',
      name: 'Test',
      component: Test,
    },
    {
      path: '/fandoms',
      name: 'Fandoms',
      components: {
        default: ListFandoms,
        menu: Menu,
      },
    },
    {
      path: '/authors',
      name: 'Authors',
      components: {
        default: ListAuthors,
        menu: Menu,
      },
    },
    {
      path: '/characters',
      name: 'Characters',
      components: {
        default: ListCharacters,
        menu: Menu,
      },
    },
    {
      path: '/ships',
      name: 'Ships',
      components: {
        default: ListShips,
        menu: Menu,
      },
    },
    {
      path: '/works',
      name: 'Works',
      components: {
        default: ListWorks,
        menu: Menu,
      },
    },
  ],
});
