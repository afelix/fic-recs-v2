import Api from './Api';

export default {
  fetchAuthor(id) {
    return Api().get(`/author/${id}`);
  },
  fetchAuthors() {
    return Api().get('/authors');
  },
  createAuthor(data, token) {
    return Api().post('/authors', data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  editAuthor(id, data, token) {
    return Api().put(`/author/${id}`, data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  deleteAuthor(id, token) {
    return Api().delete(`/author/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
};
