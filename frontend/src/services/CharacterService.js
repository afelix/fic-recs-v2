import Api from './Api';

export default {
  fetchCharacter(id) {
    return Api().get(`/character/${id}`);
  },
  fetchCharacters() {
    return Api().get('/characters');
  },
  createCharacter(data, token) {
    return Api().post('/characters', data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  editCharacter(id, data, token) {
    return Api().put(`/character/${id}`, data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  deleteCharacter(id, token) {
    return Api().delete(`/character/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
};
