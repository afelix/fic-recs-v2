import Api from './Api';

export default {
  fetchFandom(id) {
    return Api().get(`/fandom/${id}`);
  },
  fetchFandoms() {
    return Api().get('/fandoms');
  },
  createFandom(data, token) {
    return Api().post('/fandoms', data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  editFandom(id, data, token) {
    return Api().put(`/fandom/${id}`, data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  deleteFandom(id, token) {
    return Api().delete(`/fandom/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
};
