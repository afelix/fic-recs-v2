import Api from './Api';

export default {
  login(username, password) {
    const authorization = btoa(`${username}:${password}`);
    return Api().post('login', {}, {
      headers: {
        Authorization: `Basic ${authorization}`,
        'Content-Type': 'application/json',
      },
    });
  },
};
