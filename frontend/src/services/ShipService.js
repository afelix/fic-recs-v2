import Api from './Api';

export default {
  fetchShips() {
    return Api().get('/ships');
  },
  fetchShipsFiltered(filter_) {
    return Api().get('/ships', {
      params: filter_,
    });
  },
  fetchShip(id) {
    return Api().get(`/ship/${id}`);
  },
  createShip(data, token) {
    return Api().post('/ships', data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  editShip(id, data, token) {
    return Api().put(`/ship/${id}`, data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  deleteShip(id, token) {
    return Api().delete(`/ship/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
};
