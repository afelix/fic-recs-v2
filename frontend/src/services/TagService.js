import Api from '@/services/Api';

export default {
  fetchTags() {
    return Api().get('/tags');
  },
  fetchTag(id) {
    return Api().get(`/tag/${id}`);
  },
  createTag(data, token) {
    return Api().post('/tags', data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  editTag(id, data, token) {
    return Api().put(`/tag/${id}`, data, {
      headers: {
        Authorization: `Bearer: ${token}`,
      },
    });
  },
  deleteTag(id, token) {
    return Api().delete(`/tag/${id}`, {
      headers: {
        Authorization: `Bearer: ${token}`,
      },
    });
  },
};
