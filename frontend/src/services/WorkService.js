import Api from './Api';

export default {
  fetchWorks() {
    return Api().get('/works');
  },
  fetchWork(id) {
    return Api().get(`/work/${id}`);
  },
  createWork(data, token) {
    return Api().post('/works', data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  editWork(id, data, token) {
    return Api().put(`/work/${id}`, data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  deleteWork(id, token) {
    return Api().delete(`/work/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
};
