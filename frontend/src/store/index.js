import Vue from 'vue';
import Vuex from 'vuex';

import * as actions from './actions';
import user from './modules/user';
import fandom from './modules/fandom';

const debug = process.env.NODE_ENV !== 'production';

Vue.use(Vuex);

export default new Vuex.Store({
  actions,
  modules: {
    user,
    fandom,
  },
  strict: debug,
});
