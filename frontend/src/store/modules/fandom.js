// Initial state
const stateInitial = {
  // temporary storage for moving variables between components
  editCreate: {
    id: '',
    item: {},
  },
};

const getters = {
  editId: state => state.editCreate.id,
  editItem: state => state.editCreate.item,
};

/* eslint-disable no-param-reassign */
const mutations = {
  setEditId(state, { id }) {
    state.editCreate.id = id;
  },
  setEditItem(state, { item }) {
    state.editCreate.item = item;
  },
};

const actions = {
  // pass
};

export default {
  namespaced: true,
  state: stateInitial,
  getters,
  mutations,
  actions,
};
