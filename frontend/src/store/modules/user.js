// Initial state
const stateInitial = {
  name: '',
  scope: null,
  token: '',
};

const getters = {
  token: state => state.token,
  loggedIn: state => state.name !== '',
};

const actions = {
  // pass
};

/* eslint-disable no-param-reassign */
const mutations = {
  login(state, payload) {
    state.token = payload.token;
    state.scope = payload.scope;
    state.name = payload.name;
  },
};

export default {
  namespaced: true,
  state: stateInitial,
  getters,
  actions,
  mutations,
};
